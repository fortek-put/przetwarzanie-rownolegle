#include <cstdio>
#include <omp.h>

#define LOGICAL_CORE_COUNT 8    // sysctl -n hw.ncpu
#define PHYSICAL_CORE_COUNT 4   // sysctl -n hw.physicalcpu

long long num_steps = 1000000000;
double step;

int main(int argc, char* argv[])
{
    double start, stop;
    double x, pi, sum = 0.0;
    int i;
    step = 1. / (double) num_steps;
    start = omp_get_wtime();

    for (i = 0; i < num_steps; i++) {
        x = (i + .5) * step;
        sum = sum + 4.0 / (1. + x * x);
    }

    pi = sum * step;
    stop = omp_get_wtime();

    printf("Wartosc liczby PI wynosi %15.12f\n", pi);
    printf("Czas przetwarzania wynosi %f sekund\n", stop - start);
    return 0;
}
