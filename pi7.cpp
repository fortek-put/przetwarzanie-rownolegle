#include <cstdio>
#include <omp.h>

#define LOGICAL_CORE_COUNT 8    // sysctl -n hw.ncpu
#define PHYSICAL_CORE_COUNT 4   // sysctl -n hw.physicalcpu

long long num_steps = 1000000000;
double step;

int main(int argc, char* argv[])
{
    double start, stop;
    double pi, sum = 0.0;
    volatile double local_sums[50] = {};
    int num_threads = 2;

    step = 1. / (double) num_steps;

    omp_set_num_threads(num_threads);

    for (int offset = 0; offset < 50; offset++) {
        sum = 0.0;
        for (volatile double &local_sum : local_sums) {
            local_sum = 0;
        }

        start = omp_get_wtime();

        #pragma omp parallel
        {
            int id = omp_get_thread_num();
            int position = id + offset;

            #pragma omp for
            for (int i = 0; i < num_steps; i++) {
                double x = (i + .5) * step;
                local_sums[position] += 4.0 / (1. + x * x);
            }

            #pragma omp atomic
            sum += local_sums[position];
        }

        stop = omp_get_wtime();

        printf("%d %f\n", offset, stop - start);
    }

    return 0;
}
