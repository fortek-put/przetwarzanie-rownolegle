#include <cstdio>
#include <cmath>
#include <cstring>
#include <omp.h>
#include <vector>

#define PHYSICAL_CORE_COUNT 2
#define LOGICAL_CORE_COUNT 4

#define THREAD_COUNT 2
#define BLOCK_SIZE 1048576

bool* create_is_primes(std::uint64_t range_to);
std::pair<int, int*> find_base_primes(std::uint64_t greatest_prime_to_consider, bool is_prime[]);
int count_primes(const bool is_prime[], std::uint64_t range_from, std::uint64_t range_to);
bool* division(std::uint64_t range_from, std::uint64_t range_to);
bool* division_parallel(std::uint64_t range_from, std::uint64_t range_to);
bool* sieve(std::uint64_t range_from, std::uint64_t range_to);
bool* sieve_parallel_functional(std::uint64_t range_from, std::uint64_t range_to);
bool* sieve_parallel_domain(std::uint64_t range_from, std::uint64_t range_to);

int main(int argc, char* argv[])
{
    std::uint64_t range_from = 100000000/2;
    std::uint64_t range_to = 100000000;

    double start, end;
    start = omp_get_wtime();

    bool* is_prime = division_parallel(range_from, range_to);

    end = omp_get_wtime();

    printf("\nExecution time: %fs", end - start);
    printf("\nFound %d primes", count_primes(is_prime, range_from, range_to));

    delete[] is_prime;

    return 0;
}

bool* create_is_primes(std::uint64_t range_to)
{
    bool* is_prime = new bool[range_to + 1];
    memset(is_prime, 1, range_to + 1);
    is_prime[0] = false;
    is_prime[1] = false;

    return is_prime;
}

std::pair<int, int*> find_base_primes(std::uint64_t greatest_prime_to_consider, bool is_prime[])
{
    int* primes = new int[greatest_prime_to_consider];
    int index = 0;

    for (int number = 2; number <= sqrt(greatest_prime_to_consider); number++) {
        if (!is_prime[number]) {
            continue;
        }

        for (int multiply = number * number; multiply <= greatest_prime_to_consider; multiply += number) {
            is_prime[multiply] = false;
        }
    }

    for (int number = 2; number <= greatest_prime_to_consider; number++) {
        if (is_prime[number]) {
            *(primes + index++) = number;
        }
    }

    return std::pair<int, int*> {index, primes};
}

int count_primes(const bool is_prime[], std::uint64_t range_from, std::uint64_t range_to)
{
    int counter = 0;
    for (unsigned int i = range_from; i <= range_to; i++) {
        if (is_prime[i]) {
            counter++;
        }
    }

    return counter;
}

bool* division(std::uint64_t range_from, std::uint64_t range_to)
{
    bool* is_prime = create_is_primes(range_to);
    std::pair<int, int*> primes_pair = find_base_primes(sqrt(range_to), is_prime);
    int primes_count = primes_pair.first;
    int* primes = primes_pair.second;

    uint64_t number = std::min(range_from, (std::uint64_t) 3);
    for (; number <= range_to; number++) {
        for (int prime_index = 0; prime_index < primes_count; prime_index++) {
            if (number % primes[prime_index] == 0 && number != primes[prime_index]) {
                is_prime[number] = false;
                break;
            }
        }
    }

    return is_prime;
}

bool* division_parallel(std::uint64_t range_from, std::uint64_t range_to)
{
    bool* is_prime = create_is_primes(range_to);
    std::pair<int, int*> primes_pair = find_base_primes(sqrt(range_to), is_prime);

    int primes_count = primes_pair.first;
    int* primes = primes_pair.second;

    omp_set_num_threads(THREAD_COUNT);
    #pragma omp parallel
    {
        uint64_t start = std::min(range_from, (std::uint64_t) 3);
        #pragma omp for schedule(dynamic)
        for (uint64_t number = start; number <= range_to; number++) {
            for (int prime_index = 0; prime_index < primes_count; prime_index++) {
                if (number % primes[prime_index] == 0 && number != primes[prime_index]) {
                    is_prime[number] = false;
                    break;
                }
            }
        }
    }

    return is_prime;
}

bool* sieve(std::uint64_t range_from, std::uint64_t range_to)
{
    bool* is_prime = create_is_primes(range_to);
    std::pair<int, int*> primes_pair = find_base_primes(sqrt(range_to), is_prime);
    int primes_count = primes_pair.first;
    int* primes = primes_pair.second;

    for (std::uint64_t block_start = range_from; block_start <= range_to; block_start += BLOCK_SIZE) {
        std::uint64_t block_end = std::min(block_start + BLOCK_SIZE, range_to);
        std::uint64_t largest_prime_to_consider = sqrt(block_end);

        for (std::uint64_t prime_index = 0; prime_index < primes_count && primes[prime_index] <= largest_prime_to_consider; prime_index++) {
            std::uint64_t composite = block_start - block_start % primes[prime_index] + primes[prime_index];
            for (; composite <= block_end; composite += primes[prime_index]) {
                if (composite == primes[prime_index]) {
                    continue;
                }

                is_prime[composite] = false;
            }
        }
    }

    delete[] primes;

    return is_prime;
}

bool* sieve_parallel_functional(std::uint64_t range_from, std::uint64_t range_to)
{
    bool* is_prime = create_is_primes(range_to);
    std::pair<int, int*> primes_pair = find_base_primes(sqrt(range_to), is_prime);
    int primes_count = primes_pair.first;
    int* primes = primes_pair.second;

    omp_set_num_threads(THREAD_COUNT);
    #pragma omp parallel for schedule(dynamic) default(none) shared(primes_count, range_from, primes, range_to, is_prime)
    for (std::uint64_t block_start = range_from; block_start <= range_to; block_start += BLOCK_SIZE) {
        std::uint64_t block_end = std::min(block_start + BLOCK_SIZE, range_to);
        std::uint64_t largest_prime_to_consider = sqrt(block_end);

        bool* local_is_prime = create_is_primes(block_end);

        for (std::uint64_t prime_index = 0; prime_index < primes_count; prime_index++) {
            if (primes[prime_index] > largest_prime_to_consider) continue;
            std::uint64_t composite = block_start - block_start % primes[prime_index] + primes[prime_index];
            for (; composite <= block_end; composite += primes[prime_index]) {
                if (composite == primes[prime_index]) {
                    continue;
                }

                local_is_prime[composite] = false;
            }
        }

        for (std::uint64_t number = block_start; number <= block_end; number++) {
            if (!local_is_prime[number]) {
                is_prime[number] = false;
            }
        }

        delete[] local_is_prime;
    }

    delete[] primes;

    return is_prime;
}

bool* sieve_parallel_domain(std::uint64_t range_from, std::uint64_t range_to)
{
    bool* is_prime = create_is_primes(range_to);

    omp_set_num_threads(THREAD_COUNT);
    #pragma omp parallel for schedule(dynamic) default(none) shared(range_from, range_to, is_prime)
    for (std::uint64_t block_start = range_from; block_start <= range_to; block_start += BLOCK_SIZE) {
        std::uint64_t block_end = std::min(block_start + BLOCK_SIZE, range_to);
        std::uint64_t largest_prime_to_consider = sqrt(block_end);

        std::pair<int, int*> primes_pair = find_base_primes(largest_prime_to_consider, is_prime);
        int primes_count = primes_pair.first;
        int* primes = primes_pair.second;

        for (std::uint64_t prime_index = 0; prime_index < primes_count && primes[prime_index] <= largest_prime_to_consider; prime_index++) {
            std::uint64_t composite = block_start - block_start % primes[prime_index] + primes[prime_index];
            for (; composite <= block_end; composite += primes[prime_index]) {
                if (composite == primes[prime_index]) {
                    continue;
                }

                is_prime[composite] = false;
            }
        }

        delete[] primes;
    }

    return is_prime;
}
